/** TOCHR Core v. 1.0.0
 *   Last edit: 07.07.2015
 * */
var TOCHR = {};

TOCHR.App = function (id, activePageId, debugMode, onBindSelector, onEventTypes, thumbSilderSelector, PRApiActive)
{
    this.id = (id) ? id : "default";
    this.activePageId = (activePageId) ? activePageId : null;
    this.debugMode = (debugMode) ? debugMode : false;
    this.onBindSelector = (onBindSelector) ? onBindSelector : "#app-default";
    this.onEventTypes = (onEventTypes) ? onEventTypes : "click touch";
    this.thumbSliderSelector = (thumbSilderSelector) ? thumbSilderSelector : "#slides .slide-nav";
    this.PRApiActive = (PRApiActive) ? PRApiActive : false;

    this.pagesCount = 0;
    this.pages = {};
    this.ziop = {};
};

TOCHR.App.prototype.init = function ()
{
    self = this;
    // Images preload
    if (typeof imagesForPreload != 'undefined' && TOCHR.isArray(imagesForPreload)) {
        alert("Loading images");
        TOCHR.preloadImages(imagesForPreload);
        alert("Images loaded.");
    }

    // Click on every button
    // TODO: get rid of jquery
    $(this.onBindSelector).on(this.onEventTypes, ".button", function () {
        self.navigate($(this).attr("data-target-id"), this);
    });

    $(this.onBindSelector).on(this.onEventTypes, ".menu-button", function () {
        $(".menu-button").removeClass("active");
        $(this).addClass("active");
        self.navigate($(this).attr("data-target-id"), this);
    });

    $(this.onBindSelector).on(this.onEventTypes, ".arrow-left", function () {
        self.navigate(self.pages[self.activePageId].getPrevId(), this);
    });

    $(this.onBindSelector).on(this.onEventTypes, ".arrow-right", function () {
        self.navigate(self.pages[self.activePageId].getNextId(), this);
    });

    $(this.onBindSelector).on(this.onEventTypes, ".popup-button", function () {
        self.showPopup($(this).attr("data-popup-id"));
    });

    $(this.onBindSelector).on(this.onEventTypes, ".close-popup", function () {
        self.closePopup(this);
    });

    // Side menu
    $(this.onBindSelector).on(this.onEventTypes, '.side-menu .arrow', function () {
        if ($(".side-menu").css("right") == "0px") {
            $(".side-menu").animate({right: "-131px"}, 400);
        } else {
            $(".side-menu").animate({right: "0px"}, 400);
        }
    });

    // Reference close
    $(this.onBindSelector).on(this.onEventTypes, '.reference .close-reference', function () {
        if (this.PRApiActive)
            PRApi.addLog(self.id, self.activePageId, 'reference', 'close');
        $("#" + self.activePageId + " .reference").hide();
        self.hideOverlay();
    });

    // Shortlist close
    $(".close-shortlist").on(this.onEventTypes, function () {
        if (this.PRApiActive)
            PRApi.addLog(self.id, self.activePageId, 'shortlist', 'close');
        $(this).closest(".shortlist").hide();
        self.hideOverlay();
    });

    $("#external-content .close").click(function () {
        $("#external-content object").attr("data", "");
        $("#external-content").hide();
    });

    // Init custom init
    this.customInit();
    // Show activePage by activePageId
    this.showPage(this.activePageId);
};

TOCHR.App.prototype.navigate = function (targetId, element)
{
    selfNavigate = this;
    if (targetId) {
        switch (targetId) {
            case "pharma-reader":
                console.log("Pharma-reader not implemented.");
                break;
            case "slides":
                $(".reference").hide();
                this.resetSPC();
                this.resetShortlist();
                if ($("#slides").css("display") == "none") {
                    $('.footer [data-target-id="slides"]').addClass("active");
                    this.showOverlay();
                    $("#slides").show();
                } else {
                    $('.footer [data-target-id="slides"]').removeClass("active");
                    this.hideOverlay();
                    $("#slides").hide();
                }
                break;
            case "slides-close":
                this.resetSlides();
                break;
            case "reload":
                this.navigate(this.activePageId);
                break;
            case "spc":
                $(".reference").hide();
                this.resetSlides();
                this.resetShortlist();
                if ($("#spc_zip").css("display") == "none") {
                    $('.footer [data-target-id="spc"]').addClass("active");
                    this.showOverlay();
                    $("#spc_zip").show();
                } else {
                    $('.footer [data-target-id="spc"]').removeClass("active");
                    this.hideOverlay();
                    $("#spc_zip").hide();
                }
                break;
            case "references":
                $(".popup").hide();
                this.resetSPC();
                this.resetShortlist();
                if ($("#" + this.activePageId + " .reference").length > 0) {
                    if (typeof this.pages[this.activePageId].references === 'function') {
                        this.pages[this.activePageId].references();
                    }
                    else if ($("#" + this.activePageId + " .reference").css("display") == "none") {
                        this.showOverlay();
                        $("#" + this.activePageId + " .reference").show();
                    }
                }
                break;
            case "shortlist":
                $(".reference").hide();
                this.resetSPC();
                this.resetSlides();
                if ($("#" + this.activePageId).closest(".group").children(".shortlist").length != 0) {
                    id = $("#" + this.activePageId).closest(".group").attr("id");
                    this.showShortlist(id);
                    $('.footer [data-target-id="shortlist"]').addClass("active");
                    if (this.PRApiActive)
                        PRApi.addLog(this.id, this.activePageId, 'shortlist', 'show', id);
                }
                break;
            case "pen":
                if ($("#painting").css("display") !== "none") {
                    $('[data-target-id="pen"]').removeClass("active");
                    $("#painting").hide();
                    if (this.PRApiActive)
                        PRApi.addLog(this.id, this.activePageId, 'Pen', 'hide');
                } else {
                    $('[data-target-id="pen"]').addClass("active");
                    initPainting();
                    $("#painting").show();
                    if (this.PRApiActive)
                        PRApi.addLog(this.id, this.activePageId, 'Pen', 'show');
                }
                break;
            case "global-spc":
                $("#ziop_spc").hide();
                url = "./spc/index.html";
                $("#external-content object").attr("data", url);
                $("#external-content object").attr("width", "1024px");
                $("#external-content object").attr("height", "26132px");
                $('#external-content object').load(url);
                $("#external-content").show();
                if (this.PRApiActive)
                    PRApi.addLog(this.id, this.activePageId, 'Spc', 'show', ziopCode);
                break;
            case "external-close":
                $("#external-content").hide();
                this.hideOverlay();
                if (this.PRApiActive)
                    PRApi.addLog(this.id, this.activePageId, 'External', 'close');
                break;
            case "ziop":
                this.showOverlay();
                $("#ziop_spc").show();
                break;
//            case "ostatni":
//                if (!$(".menu .ostatni").hasClass("open")) {
//                    $(".menu .ostatni").addClass("open");
//                    $(".menu .ostatni .ostatni-submenu").show();
//                } else {
//                    $(".menu .ostatni").removeClass("open");
//                    $(".menu .ostatni .ostatni-submenu").hide();
//                }
//                break;
            case "pdf":
                pdf = $(element).attr("data-target-pdf");
                height = $(element).attr("data-target-pdf-height");
                if (pdf) {

                    url = "./pdf/" + pdf;
                    $("#pdf object").attr("data", url);
                    $("#pdf object").attr("width", "1024px");
                    $("#pdf object").attr("height", (height)?height:"20000px");
                    $('#pdf object').load(url);
                    $("#pdf").show();
                    $('#pdf .content').scrollTop(0);
                    
                    if (this.PRApiActive)
                        PRApi.addLog(this.id, this.activePageId, 'PDF', 'show', ziopCode);
                }
                break;
            case "pdf-close":
                $("#pdf").hide();
                if (this.PRApiActive)
                    PRApi.addLog(this.id, this.activePageId, 'PDF', 'close');
                break;
            default:
                pageId = targetId;
                if ($("#" + pageId).length == 0) {
                    if (this.debugMode)
                        console.log("Target id for navigation doesnt exist.");
                } else {
                    // Resets
                    $(".side-menu").css("right", "-131px");
                    $(".popup").hide();
                    $(".reference").hide();
                    this.resetSPC();
                    this.resetSlides();
                    this.resetShortlist();
                    // Prevent several animations
                    clearTimeouts();
                    $("#" + this.activePageId + " *").each(function () {
                        $(this).stop();
                    });
                    // Hide overlay
                    this.hideOverlay();
                    $("#slides").hide();
                    // Navigation
                    if (this.activePageId == pageId) {
                        this.pages[pageId].reset();
                        this.pages[pageId].init();
                        this.pages[pageId].animate();
                    } else {
                        oldActivePageId = this.activePageId;
                        this.activePageId = pageId;
                        this.showPage(pageId);
                        this.pages[oldActivePageId].reset();
                    }
                    // Recreate footer slider
                    this.createThumbSlider();
                    // Setting menu button active
                    $(".menu-button").removeClass("active");
                    $('.menu-button[data-group-id="' + $("#" + this.activePageId).parent().attr("id") + '"]').addClass("active");
                }
                break;
        }
    } else {
        if (this.debugMode)
            console.log("No target id passed to method navigate.");
    }
};

TOCHR.App.prototype.resetSPC = function ()
{
    $('.footer [data-target-id="spc"]').removeClass("active");
    $("#ziop_spc").hide();
    this.hideOverlay();
};

TOCHR.App.prototype.resetSlides = function ()
{
    $('.footer [data-target-id="slides"]').removeClass("active");
    $("#slides").hide();
    this.hideOverlay();
};

TOCHR.App.prototype.resetShortlist = function ()
{
    $('.footer [data-target-id="shortlist"]').addClass("active");
    this.hideOverlay();
    $(".shortlist").hide();
};

TOCHR.App.prototype.showShortlist = function (pageId)
{
    this.showOverlay();
    $("#" + pageId + " .shortlist").show();
};

TOCHR.App.prototype.showPopup = function (popupId)
{
    this.showOverlay();
    $(".popup").hide();
    $("#" + popupId).show();
    if (this.PRApiActive)
        PRApi.addLog(this.id, this.activePageId, popupId, 'show');
};

TOCHR.App.prototype.closePopup = function (element)
{
    $(element).parent().hide();
    this.hideOverlay();
    if (this.PRApiActive)
        PRApi.addLog(this.id, this.activePageId, $(element).parent().attr("id"), 'close');
};

TOCHR.App.prototype.showOverlay = function ()
{
    document.getElementById("overlay").style.display = "block";
};

TOCHR.App.prototype.hideOverlay = function ()
{
    document.getElementById("overlay").style.display = "none";
};

TOCHR.App.prototype.customInit = function ()
{

};

TOCHR.App.prototype.addNewPage = function (page)
{
    this.pages[page.getId()] = page;
    this.pagesCount += 1;
};

TOCHR.App.prototype.setActivePageId = function (activePageId)
{
    this.activePageId = activePageId;
};

TOCHR.App.prototype.showPage = function (pageId)
{
    this.hideAllPages();
    document.getElementById(pageId).style.display = "block";
    this.pages[pageId].reset();
    this.pages[pageId].init();
    this.pages[pageId].animate();
    if (this.PRApiActive)
        PRApi.addLog(this.id, pageId, 'main', 'init');
};

TOCHR.App.prototype.hideAllPages = function ()
{
    pages = document.getElementsByClassName("page");
    var i;
    for (i = 0; i < pages.length; i++) {
        pages[i].style.display = "none";
    }
};

TOCHR.App.prototype.createThumbSlider = function ()
{
    selfThumb = this;
    // Reset of content
    $(this.thumbSliderSelector).html("");
    // Contains thumbs
    $(this.thumbSliderSelector).append('<div id="carousel" class="scrollable"><ul></ul></div>');
    if (1) {
        $(".page").each(function () {
            if (this.debugMode)
                console.log(this);
            id = $(this).attr("id");
            thumb = '<li class="button" data-target-id="' + id + '">\n';
            thumb += '<span class="thumb" style="background: url(\'./thumbs/' + id + '.png\') no-repeat; -webkit-background-size: 135px 88px; background-size: 135px 88px;"></span>';
//            if (selfThumb.pages[id].getThumbText()) {
//                thumb += '<span class="number">' + selfThumb.pages[id].getThumbText() + '</span>';
//            }
            thumb += '</li>';
            $(selfThumb.thumbSliderSelector + " ul").append(thumb);
        });

        $('#carousel').carousel('#previous', '#next', true);
    } else {
        groupId = $("#" + this.activePageId).parent().attr("id");
        $("#" + groupId + " .page").each(function () {
            if (this.debugMode)
                console.log(this);
            id = $(this).attr("id");
            thumb = '<li class="button" data-target-id="' + id + '">\n';
            thumb += '<span class="thumb" style="background: url(\'./thumbs/' + id + '.png\') no-repeat; -webkit-background-size: 135px 88px; background-size: 135px 88px;"></span>';
//            if (selfThumb.pages[id].getThumbText()) {
//                thumb += '<span class="number">' + selfThumb.pages[id].getThumbText() + '</span>';
//            }
            thumb += '</li>';
            $(selfThumb.thumbSliderSelector + " ul").append(thumb);
        });
        if ($("#" + groupId + " .page").length > 4) {
            // Init slider if there is more then 4 slides
            $('#carousel').carousel('#previous', '#next', true);
        }
    }
};

TOCHR.App.prototype.setZiop = function (ziop)
{
    this.ziop = ziop;
};

TOCHR.App.prototype.getZiopByCode = function (code)
{
    return (this.ziop[code] != undefined) ? this.ziop[code] : null;
};

TOCHR.Page = function (id) {
    this.id = id;
    this.prevId = null;
    this.nextId = null;
    this.groupId = null;
    this.thumbText = null;
    this.ziopCode = null;

};

TOCHR.Page.prototype.init = function ()
{
    this.customInit();
};

TOCHR.Page.prototype.customInit = function ()
{

};


TOCHR.Page.prototype.show = function ()
{

};

TOCHR.Page.prototype.hide = function ()
{

};

TOCHR.Page.prototype.animate = function ()
{

};

TOCHR.Page.prototype.reset = function ()
{

};

TOCHR.Page.prototype.setPrevId = function (prevId)
{
    this.prevId = prevId;
};

TOCHR.Page.prototype.getPrevId = function ()
{
    return this.prevId;
};

TOCHR.Page.prototype.setNextId = function (nextId)
{
    this.nextId = nextId;
};

TOCHR.Page.prototype.getNextId = function ()
{
    return this.nextId;
};

TOCHR.Page.prototype.setThumbText = function (thumbText)
{
    this.thumbText = thumbText;
};

//TOCHR.Page.prototype.getThumbText = function ()
//{
//    return this.thumbText;
//};

TOCHR.Page.prototype, setGroupId = function (groupId)
{

};

TOCHR.Page.prototype.getId = function ()
{
    return this.id;
};

TOCHR.Page.prototype.setZiopCode = function (ziopCode)
{
    this.ziopCode = ziopCode;
};

TOCHR.Page.prototype.getZiopCode = function ()
{
    return this.ziopCode;
};

/* Function for preloading of images given in array */
TOCHR.preloadImages = function (images) {
    var imagesPreloaded = new Array()
    for (i = 0; i < images.length; i++) {
        imagesPreloaded[i] = new Image()
        imagesPreloaded[i].src = images[i]
    }
}

/* Function - Is Array? */
TOCHR.isArray = function (myArray) {
    return myArray.constructor.toString().indexOf("Array") > -1;
};

// Painting
var canvas, stage;
var drawingCanvas;
var oldPt;
var oldMidPt;
var title;
var color;
var stroke;
var colors;
var index;

function initPainting() {
    canvas = document.getElementById("painting");
    canvas.width = canvas.width;
    index = 0;
    colors = "#fc1a1a";

    //check to see if we are running in a browser with touch support
    stage = new createjs.Stage(canvas);
    stage.autoClear = false;
    stage.enableDOMEvents(true);

    createjs.Touch.enable(stage);
    createjs.Ticker.setFPS(24);

    drawingCanvas = new createjs.Shape();

    stage.addEventListener("stagemousedown", handleMouseDown);
    stage.addEventListener("stagemouseup", handleMouseUp);

    stage.addChild(drawingCanvas);
    stage.update();
}

function stop() {
}

function handleMouseDown(event) {
    color = colors;
    stroke = 3;
    oldPt = new createjs.Point(stage.mouseX, stage.mouseY);
    oldMidPt = oldPt;
    stage.addEventListener("stagemousemove", handleMouseMove);
}

function handleMouseMove(event) {
    var midPt = new createjs.Point(oldPt.x + stage.mouseX >> 1, oldPt.y + stage.mouseY >> 1);

    drawingCanvas.graphics.clear().setStrokeStyle(stroke, 'round', 'round').beginStroke(color).moveTo(midPt.x, midPt.y).curveTo(oldPt.x, oldPt.y, oldMidPt.x, oldMidPt.y);

    oldPt.x = stage.mouseX;
    oldPt.y = stage.mouseY;

    oldMidPt.x = midPt.x;
    oldMidPt.y = midPt.y;

    stage.update();
}

function handleMouseUp(event) {
    stage.removeEventListener("stagemousemove", handleMouseMove);
}


/* Overides*/
/* Set timeout override */
var timeouts = [];
window.oldSetTimeout = window.setTimeout;

window.setTimeout = function (func, delay) {
    timeout = window.oldSetTimeout(function () {
        try {
            func();
        }
        catch (exception) {
            console.log(exception);
        }
    }, delay);
    timeouts.push(timeout);
    return timeout;
};

function clearTimeouts() {
    $.each(timeouts, function (key, value) {
        clearTimeout(value);
    });
    timeouts = null;
    timeouts = [];
}
