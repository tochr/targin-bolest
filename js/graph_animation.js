/*
 Graph animation
 @Author Nirmala, Ratnakar, Lalitha
 */

//Variable declaration
var button1 = "off";
var button2 = "off";
var button3 = "off";
var button4 = "off";
var button5 = "off";
// define touch/click variable
var touchOrClick = Modernizr.touch ? 'touchstart' : 'click';

$(document).ready(function () {
    initVal();
    textFieldEvents();
    txtValFun();

});

//Initialization
//------------------------------------
function initVal() {
    $("#systolic_chat_redlines").hide();
    $("#diastolic_chart_redlines").hide();
    $("#cholesterol_chart_redlines").hide();
    $("#weight_chart_redlines").hide();
    $("#weightParagraph").addClass("weightPara_off");
    yAxis = $("#yAxis");
}

//To handle all text fields enter event
function textFieldEvents() {
    $('#txtFirst1, #txtFirst2, #txtSecond1, #txtSecond2, #txtThird1, #txtThird2, #txtFourth1, #txtFourth2, #txtFifth1, #txtFifth2').keypress(function () {
        var dest = $(this);
        dest.val(dest.val().split(" ").join(""));
    });
}

//All graph text field functionality & corresponding click event functionality.
//-----------------------------------------------------------------------------
function txtValFun() {
    txtValFun1();
    txtValFun2();
    txtValFun3();
    txtValFun4();
    txtValFun5();
}

//First (cortisol) txtbox keypress event.
//----------------------------------------
function txtValFun1() {
    $('#txtFirst').keypress(function (event) {
        var allowKeys = new Array(8, 13, 39, 37);
        var dest = $(this);
        var newYVal = 0;
        dest.val(dest.val().split(" ").join(""));
        var yVal = $("#txtFirst").val();
        var keycode = (event.keyCode ? event.keyCode : event.which);
        var keyValue = yVal + "" + String.fromCharCode(keycode);
        if (!isNaN(keyValue) || allowKeys.indexOf(keycode) > -1) {
            if (keycode === 13) {
                drawJQPlotGraph(yVal);
                $('div.jqplot-yaxis').children("div:nth-child(3)").css('background', 'red');
                selectValue(yVal);
            }
        } else {
            return false;
        }
        event.stopPropagation();
    });
}

//Second (Systolic blood pressure) textbox keypress event.
//---------------------------------------------------------
function txtValFun2() {
    $('#txtSecond').keypress(function (event) {
        var allowKeys = new Array(8, 13, 39, 37);
        var dest = $(this);
        var newYVal = 0;
        dest.val(dest.val().split(" ").join(""));
        var yVal = $("#txtSecond").val();
        var keycode = (event.keyCode ? event.keyCode : event.which);
        var keyValue = yVal + "" + String.fromCharCode(keycode);
        if (!isNaN(keyValue) || allowKeys.indexOf(keycode) > -1) {
            if (keycode === 13) {
                if ($("#systolic_content").is(":visible"))
                    graph2();
            }
        } else {
            return false;
        }
        event.stopPropagation();
    });
}

//Third (Diastolic blood pressure) textbox keypress event.
//---------------------------------------------------------
function txtValFun3() {
    $('#txtThird').keypress(function (event) {
        var allowKeys = new Array(8, 13, 39, 37);
        var dest = $(this);
        var newYVal = 0;
        dest.val(dest.val().split(" ").join(""));
        var yVal = $("#txtThird").val();
        var keycode = (event.keyCode ? event.keyCode : event.which);
        var keyValue = yVal + "" + String.fromCharCode(keycode);
        if (!isNaN(keyValue) || allowKeys.indexOf(keycode) > -1) {
            if (keycode === 13) {
                if ($("#diastolic_content").is(":visible"))
                    graph3();
            }
        } else {
            return false;
        }
        event.stopPropagation();
    });
}

//Fourth (Total cholesterol) textbox keypress event.
//---------------------------------------------------------
function txtValFun4() {
    $('#txtFourth').keypress(function (event) {
        var allowKeys = new Array(8, 13, 39, 37);
        var dest = $(this);
        var newYVal = 0;
        dest.val(dest.val().split(" ").join(""));
        var yVal = $("#txtFourth").val();
        var keycode = (event.keyCode ? event.keyCode : event.which);
        var keyValue = yVal + "" + String.fromCharCode(keycode);
        if (!isNaN(keyValue) || allowKeys.indexOf(keycode) > -1) {
            if (keycode === 13) {
                if ($("#cholesterol_content").is(":visible"))
                    graph4();
            }
        } else {
            return false;
        }
        event.stopPropagation();
    });
}

//Fifth (Weight) textbox keypress event.
//---------------------------------------------------------
function txtValFun5() {
    $('#txtFifth').keypress(function (event) {
        var allowKeys = new Array(8, 13, 39, 37);
        var dest = $(this);
        var newYVal = 0;
        dest.val(dest.val().split(" ").join(""));
        var yVal = $("#txtFifth").val();
        var keycode = (event.keyCode ? event.keyCode : event.which);
        var keyValue = yVal + "" + String.fromCharCode(keycode);
        if (!isNaN(keyValue) || allowKeys.indexOf(keycode) > -1) {
            if (keycode === 13) {
                if ($("#weight_content").is(":visible"))
                    graph5();
            }
        } else {
            return false;
        }
        event.stopPropagation();
    });
}


//Common function to empty all graph div's
//----------------------------------------------
function emptyGraphDiv() {
    initVal();
    $("#chart5Div").empty();
}


//Graph5 Funtion:
function graph5() {

    button1 = "off";
    button2 = "off";
    button3 = "off";
    button4 = "off";
    button5 = "on";
    emptyGraphDiv();
    var yVal = "";
    if ($.trim(yVal) == "") {
        yVal = 0;
    }
    yVal = drawYAxis(yVal, 94.0, 70, 12, 232.0, 2, $("#chart5Div")[0]);

    drawRaphelGraph(12, 129, 44, 294, 140, 294, 399, 294, 310, 428, "chart5Div", 12, 294, 485, 310, "txtFifth");
}

function getPixelFromPoint(pointVal) {
    return Math.round(pixelDiff * (maxYAxisValue - pointVal) + 15);
}


//Raphel plugin for drag & drop funcionality
/*code is used for 
 * 1. Graph 1 (Systolic blood pressure), 2. Graph 2 (Systolic blood pressure ), 3. Graphr 3(Diastolic blood pressure), 4. Graph4 (Total cholesterol) and 5. Graph 5(Weight)
 */
function drawRaphelGraph(x1, y1, x2, y2, x3, y3, x4, y4, height, width, chartName, minLimit, MaxLimit, otherLimit, recH, txtBoxValue) {
    var r = Raphael(chartName, 428, recH),
//            discattr = {fill: "#9698AB", stroke: "none"};
            discattr = {fill: 'url("./img/circle.png")', stroke: "none"};
//    r.rect(0, 0, height, width, 10).attr({stroke: "#F2F2F2"});
    r.rect(0, 0, height, width, 10).attr({stroke: "none"});

    function curve(x, y, ax, ay, bx, by, zx, zy, color) {
        var path = [["M", x, y], ["L", ax, ay, bx, by, zx, zy]],
                path2 = [["L", x, y], ["L", ax, ay], ["L", bx, by], ["L", zx, zy]],
                curve = r.path(path).attr({stroke: color || Raphael.getColor(), "stroke-width": 4, "stroke-linecap": "round"}),
                controls = r.set(
                        r.path(path2).attr({stroke: "#9698AB", "stroke-dasharray": ". "}),
                        r.circle(x, y, 12).attr(discattr),
                        r.circle(ax, ay, 12).attr(discattr),
                        r.circle(bx, by, 12).attr(discattr),
                        r.circle(zx, zy, 12).attr(discattr)
                        );
        controls[1].update = function (x, y) {
            var X = 0,
                    Y = this.attr("cy") + y;
            $("#hiddenValue").val(Y);
            graphY = updateGraphValueTextBox(txtBoxValue, Y)
            $("#" + txtBoxValue).val(graphY);
            X = minLimit;
            if (Y < minLimit + 2) {
                Y = minLimit;
            }
            if (Y > MaxLimit) {
                Y = MaxLimit;
            }

            this.attr({cx: X, cy: Y});
            path[0][1] = X;
            path[0][2] = Y;
            path2[0][1] = X;
            path2[0][2] = Y;
            curve.attr({path: path});
        };
        controls[2].update = function (x, y) {
            $("#popup-setrnost .arrow.first").hide();
            var X = this.attr("cx") + x,
                    Y = this.attr("cy") + y;
            $("#hiddenValue").val(Y);
            //if(X<0){X=minLimit;}
            //if(X>otherLimit){X=otherLimit;}
            X = x2;
            if (Y < minLimit + 2) {
                Y = minLimit;
            }
            if (Y > MaxLimit) {
                Y = MaxLimit
            }
            this.attr({cx: X, cy: Y});
            path[1][1] = X;
            path[1][2] = Y;
            path2[1][1] = X;
            path2[1][2] = Y;
            curve.attr({path: path});
        };
        controls[3].update = function (x, y) {
            $("#popup-setrnost .arrow.second").hide();
            var X = this.attr("cx") + x,
                    Y = this.attr("cy") + y;
            $("#hiddenValue").val(Y);
            //if(X<0){X=minLimit;}
            //if(X>otherLimit){X=otherLimit;}
            X = x3;
            if (Y < minLimit + 2) {
                Y = minLimit;
            }
            if (Y > MaxLimit) {
                Y = MaxLimit
            }
            this.attr({cx: X, cy: Y});
            path[1][3] = X;
            path[1][4] = Y;
            path2[2][1] = X;
            path2[2][2] = Y;
            curve.attr({path: path});
        };
        controls[4].update = function (x, y) {
            $("#popup-setrnost .arrow.third").hide();
            var X = this.attr("cx") + x,
                    Y = this.attr("cy") + y;
            $("#hiddenValue").val(Y);
            //if(X<0){X=minLimit;}
            //if(X>otherLimit){X=otherLimit;}
            X = x4
            if (Y < minLimit + 2) {
                Y = minLimit;
            }
            if (Y > MaxLimit) {
                Y = MaxLimit
            }
            this.attr({cx: X, cy: Y});
            path[1][5] = X;
            path[1][6] = Y;
            path2[3][1] = X;
            path2[3][2] = Y;
            curve.attr({path: path});
        };
        controls.drag(move, up);
    }
    function move(dx, dy) {
        //console.log(this.dy);
        this.update(dx - (this.dx || 0), dy - (this.dy || 0));
        this.dx = dx;
        this.dy = dy;
    }
    function up() {
        this.dx = this.dy = 0;
    }
    curve(x1, y1, x2, y2, x3, y3, x4, y4, "#9698AB");

}
;

//Graph 2 Y-axis value
//---------------------------------------------
function yValueChart2(yVal) {
    return getPixelFromPoint(yVal);
}

//Graph 3 Y-axis value
//---------------------------------------------
function yValueChart3(yVal) {
    return getPixelFromPoint(yVal);
}

//Graph 4 Y-axis value
//---------------------------------------------
function yValueChart4(yVal) {
    return getPixelFromPoint(yVal);
}

//Graph 5 Y-axis value
//---------------------------------------------
function yValueChart5(yVal) {
    return getPixelFromPoint(yVal);
}
// Capture the parameter which itself calls respective textBox function
//----------------------------------------------------
function updateGraphValueTextBox(textBoxValue, yVal) {

    switch (textBoxValue) {
        case "txtFourth":
            var str = $(".mol_on:first").text();
            if (str == "mg/dL") {
                yVal = getPontFromPixel(yVal);
            } else {
                yVal = getPontFromPixel(yVal, 1);
            }
            break;
        default:
            yVal = getPontFromPixel(yVal);
    }
    return yVal;
}

var yAxis;
var width, height;
var minYAxisValue = 0, maxYAxisValue = 0;
var pixelDiff, pointDiff;
var canvas;
var context;
var posX, posY;
var nextPos;
var diffValue;
var minGraphVal;
var lastSelDiv = "";

function getPixelFromPoint(pointVal) {
    return Math.round(pixelDiff * (maxYAxisValue - pointVal) + 15);
}

function getPontFromPixel(pixelVal, decimalPoints) {
    decimalPoints = (typeof decimalPoints === "undefined") ? 0 : decimalPoints;
    var yValue = (maxYAxisValue - (pixelVal - 15) / pixelDiff);
    yValue = (yValue < minYAxisValue) ? 0 : ((yValue > maxYAxisValue) ? maxYAxisValue : yValue);
    return yValue.toFixed(decimalPoints);
}
function drawYAxis(selValue, maxGrahpVal, minGraphVal, diffStartEnd, graphHeight, diffPoint, graphDiv, decimalPoints) {
    yAxis.show();
    $(".graph_tiltle").show();
    selValue = parseFloat(selValue);
    //minGraphVal = minGraphVal;
    var tempMin = Math.round(selValue - diffStartEnd);

    /*if(minGraphVal % 2 != 0 && tempMin % diffPoint == 0){
     for (var i = tempMin; i > (tempMin - diffPoint); i--){
     if(i%diffPoint != 0){
     tempMin = i;
     break;
     }
     }		
     } else if(minGraphVal % 2 == 0 && tempMin % diffPoint != 0){
     for (var i = tempMin; i > (tempMin - diffPoint); i++){
     if(i%diffPoint == 0){
     tempMin = i;
     break;
     }
     }		
     }
     */
    for (var i = minGraphVal; i < maxGrahpVal; i = i + diffPoint) {
        if (i == selValue) {
            break;
        } else if (i > selValue && selValue < (i + diffPoint)) {
            tempMin = i - 3 * diffPoint;
            break;
        }
    }



    var tempMax = Math.round(tempMin + 2 * diffStartEnd);


    if (tempMax > maxGrahpVal || selValue > maxGrahpVal) {
        selValue = (selValue > maxGrahpVal) ? maxGrahpVal : selValue;
        tempMin = maxGrahpVal - 2 * diffStartEnd;
        tempMax = maxGrahpVal;
    }

    if (tempMin < minGraphVal || selValue < minGraphVal) {
        selValue = (selValue < minGraphVal) ? ((selValue == 0) ? minGraphVal - diffPoint : minGraphVal - diffPoint / 2) : selValue;
        tempMin = minGraphVal;
        tempMax = minGraphVal + 2 * diffStartEnd;
    }

    if ((selValue > maxYAxisValue || selValue < minYAxisValue) || lastSelDiv != graphDiv.id) {
        //Default Values
        minYAxisValue = tempMin; //selValue - diffStartEnd;
        maxYAxisValue = tempMax; // selValue + diffStartEnd;	
        canvas = yAxis[0];

        context = canvas.getContext("2d");
        decimalPoints = (typeof decimalPoints === "undefined") ? 0 : decimalPoints;
        yAxis.css({top: graphDiv.offsetTop + 1, left: graphDiv.offsetLeft - 45});
        width = canvas.offsetWidth;
        height = graphHeight; // canvas.offsetHeight-50;
        pointDiff = diffPoint;//2;
        nextPos = 0;
        lineX1 = 32;
        lineY1 = 15;
        lineX2 = 40;
        lineY2 = 15;

        valueX = 25;
        valueY = 20;
        var changeHeight = (graphDiv.id == "chart5Div") ? 60 : 54;
        canvas.height = changeHeight + graphHeight;
        pixelDiff = height / (maxYAxisValue - minYAxisValue);
        //alert("pixelDiff"+pixelDiff);
        context.font = "16px Arial";
        context.fillStyle = '#949CA1';
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.beginPath();
        context.lineWidth = 2;
        context.strokeStyle = '#C3C8CB';

        diffValue = pixelDiff * pointDiff;
        for (var i = maxYAxisValue; i >= minYAxisValue; i -= pointDiff) {
            context.moveTo(lineX1, lineY1 + nextPos);
            context.lineTo(lineX2, lineY2 + nextPos);
            var yAxisPoint = i.toFixed(decimalPoints);
            valueX = (yAxisPoint.length > 2) ? 0 : 8;
            context.fillText(yAxisPoint, valueX, valueY + nextPos);
            nextPos += diffValue;
        }
        //X-Axis
        context.moveTo(lineX1, lineY1 + nextPos);
        context.lineTo(width - 13, lineY2 + nextPos - 1);
        context.fillText(0, 10, valueY + nextPos);
        //X-Axis lines
        // Start
        context.moveTo(lineX1 + 148, lineY2 + nextPos - 1);
        context.lineTo(lineX1 + 148, lineY2 + nextPos + 8);
        //context.fillText(lineX1+143,lineY2+nextPos+15,"2.0");
        context.moveTo(lineX1 + 305, lineY2 + nextPos - 1);
        context.lineTo(lineX1 + 305, lineY2 + nextPos + 8);
        //context.fillText(lineX1+305,lineY2+nextPos-1,6.0);
        context.moveTo(lineX1 + 498, lineY2 + nextPos - 1);
        context.lineTo(lineX1 + 498, lineY2 + nextPos + 8);
        //context.fillText(lineX1+498,lineY2+nextPos-1,12.0);
        //end

        //Y-Axis Left line
        context.moveTo(lineX2, lineY1);
        context.lineTo(lineX2, nextPos);
        // First Dash
        context.moveTo(lineX2 - 6, nextPos + 3);
        context.lineTo(lineX2 + 6, nextPos - 4);
        // Second Dash
        context.moveTo(lineX2 - 6, nextPos + 8);
        context.lineTo(lineX2 + 6, nextPos + 1);

        context.moveTo(lineX2, nextPos + 5);
        context.lineTo(lineX2, nextPos + 15);

        //Y-Axis Right Line
        context.moveTo(width - 12, lineY1);
        context.lineTo(width - 12, nextPos + 15);
        context.stroke();
    }
    lastSelDiv = graphDiv.id;
    return selValue;
}