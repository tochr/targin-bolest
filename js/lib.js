// Create application object
var app = new TOCHR.App(
        "app-targin", // id
        "domu", // activePage
        true, // debugMode
        "#app-targin",
        "click touch",
        false,
        false
        );
app.customInit = function () {
    $("#ziop .close-popup").unbind().click(function () {
        $("#ziop_spc").hide();
    });
};

var stage = "";
// Domu
page = new TOCHR.Page("domu");
page.setNextId("co-je-to-targin");
page.customInit = function ()
{
    $("#swiffycontainer").html("");
    if (stage)
        stage.destroy();
    stage = new swiffy.Stage(document.getElementById('swiffycontainer'),
            swiffyobject, {});
    stage.start();
};
app.addNewPage(page);

// Co je to targin
page = new TOCHR.Page("co-je-to-targin");
page.setPrevId("domu");
page.setNextId("moa");
page.customInit = function ()
{
    selfPage = this;
    $("#" + this.getId() + " .next").unbind().click(function () {
        if ($(this).css("left") == "120px") {
            $(this).css("left", "510px");
            $("#" + selfPage.getId() + " .text1").fadeIn(400);
        } else {
            $(this).hide();
            $("#" + selfPage.getId() + " .text2").fadeIn(400);
        }
    });
};
page.reset = function ()
{
    $("#" + this.getId() + " .text1").hide();
    $("#" + this.getId() + " .text2").hide();
    $("#" + this.getId() + " .next").css("left", "120px");
    $("#" + this.getId() + " .next").show();
};
app.addNewPage(page);

// MOA
page = new TOCHR.Page("moa");
page.setPrevId("co-je-to-targin");
page.setNextId("analgeticka-ucinnost");
page.customInit = function ()
{
    selfPage = this;
    $("#" + this.getId() + " .b1").unbind().click(function () {
        $("#" + selfPage.getId() + " .box").hide();
        $("#" + selfPage.getId() + " .box1").fadeIn(400);
    });
    $("#" + this.getId() + " .b2").unbind().click(function () {
        $("#" + selfPage.getId() + " .box").hide();
        $("#" + selfPage.getId() + " .box2").fadeIn(400);
    });
    $("#" + this.getId() + " .b3").unbind().click(function () {
        $("#" + selfPage.getId() + " .box").hide();
        $("#" + selfPage.getId() + " .box3").fadeIn(400);
    });
    $("#" + this.getId() + " .b4").unbind().click(function () {
        $("#" + selfPage.getId() + " .box").hide();
        $("#" + selfPage.getId() + " .box4").fadeIn(400);
    });
};
page.reset = function ()
{
    $("#" + this.getId() + " .box").hide();
    $("#" + this.getId() + " .box1").show();
};
app.addNewPage(page);

// Analgeticka ucinnost
// Analgeticka ucinnost
page = new TOCHR.Page("analgeticka-ucinnost");
page.setPrevId("moa");
page.setNextId("analgeticka-ucinnost-2");
page.reset = function ()
{

};
app.addNewPage(page);

// Analgeticka ucinnost 2
page = new TOCHR.Page("analgeticka-ucinnost-2");
page.setPrevId("analgeticka-ucinnost");
page.setNextId("analgeticka-ucinnost-3");
page.reset = function ()
{

};
app.addNewPage(page);

// Analgeticka ucinnost 3
page = new TOCHR.Page("analgeticka-ucinnost-3");
page.setPrevId("analgeticka-ucinnost-2");
page.setNextId("analgeticka-ucinnost-4");
page.customInit = function ()
{
    selfPage = this;
    $("#" + this.getId() + " .b1").unbind().click(function () {
        if ($("#" + selfPage.getId() + " .text1").css("display") == "block") {
            $("#" + selfPage.getId() + " .text1").hide();
        } else {
            $("#" + selfPage.getId() + " .text1").show();
        }
    });
    $("#" + this.getId() + " .b2").unbind().click(function () {
        if ($("#" + selfPage.getId() + " .text2").css("display") == "block") {
            $("#" + selfPage.getId() + " .text2").hide();
        } else {
            $("#" + selfPage.getId() + " .text2").show();
        }
    });

};
page.reset = function ()
{
    $("#" + this.getId() + " .text1").hide();
    $("#" + this.getId() + " .text2").hide();
};
app.addNewPage(page);

// Analgeticka ucinnost 4
page = new TOCHR.Page("analgeticka-ucinnost-4");
page.setPrevId("analgeticka-ucinnost-3");
page.setNextId("analgeticka-ucinnost-5");
page.customInit = function ()
{
    selfPage = this;
    $("#" + this.getId() + " .b1").unbind().click(function () {
        if ($("#" + selfPage.getId() + " .text1").css("display") == "block") {
            $("#" + selfPage.getId() + " .text1").hide();
        } else {
            $("#" + selfPage.getId() + " .text1").show();
        }
    });
    $("#" + this.getId() + " .b2").unbind().click(function () {
        if ($("#" + selfPage.getId() + " .text2").css("display") == "block") {
            $("#" + selfPage.getId() + " .text2").hide();
        } else {
            $("#" + selfPage.getId() + " .text2").show();
        }
    });

};
page.reset = function ()
{
    $("#" + this.getId() + " .text1").hide();
    $("#" + this.getId() + " .text2").hide();
};
app.addNewPage(page);

// Analgeticka ucinnost 5
page = new TOCHR.Page("analgeticka-ucinnost-5");
page.setPrevId("analgeticka-ucinnost-4");
page.setNextId("setrnost");
page.customInit = function ()
{
    selfPage = this;
    var click = 0;
    $("#" + this.getId() + " .popup-button").unbind().click(function () {
        $("#" + selfPage.getId() + " .body-image").addClass("final");
    });
    $("#" + this.getId() + " .next").unbind().click(function () {
        switch (click) {
            case 0:
                $("#" + selfPage.getId() + " .graph-red").animate({width: "396px"}, 1000);
                break;
            case 1:
                $("#" + selfPage.getId() + " .graph-blue").animate({width: "396px"}, 1000);
                break;
            case 2:
                $("#" + selfPage.getId() + " .next").hide();
                $("#" + selfPage.getId() + " .graph-orange").animate({width: "396px"}, 1000, function () {
                    $("#" + selfPage.getId() + " .graph-orange-text").show();
                });

                break;
        }
        click++;
    });
};
page.reset = function ()
{
    $("#" + this.getId() + " .body-image").removeClass("final");
    $("#" + this.getId() + " .next").show();
    $("#" + this.getId() + " .graph-orange-text").hide();
    $("#" + this.getId() + " .graph-red").css("width", "0px");
    $("#" + this.getId() + " .graph-blue").css("width", "0px");
    $("#" + this.getId() + " .graph-orange").css("width", "0px");
};
app.addNewPage(page);
// Setrnost
// Setrnost 1
page = new TOCHR.Page("setrnost");
page.setPrevId("analgeticka-ucinnost-5");
page.setNextId("setrnost-2");
page.customInit = function ()
{
    selfPage = this;

    $("#" + this.getId() + " .popup-button").unbind().click(function () {
        $("#" + selfPage.getId() + " .body-image").addClass("final");
        graph5();
        $("canvas").hide();

    });
    $("#" + this.getId() + " .next").unbind().click(function () {
        $(".graph-overlay").show();
        $("#" + selfPage.getId() + " .next").hide();
        $("#" + selfPage.getId() + " .graph").animate({width: "861px"}, 1600, function () {
            $("#" + selfPage.getId() + " .graph-text").show();
        });

    });
};
page.reset = function ()
{
    $("#" + this.getId() + " .body-image").removeClass("final");
    $("#" + this.getId() + " .next").show();
    $("#" + this.getId() + " .graph").css("width", "0px");
    $("#" + this.getId() + " .graph-text").hide();
    $("#" + this.getId() + " .graph-overlay").hide();
    $("#" + this.getId() + " .arrow").show();
};
app.addNewPage(page);

// Setrnost 2
page = new TOCHR.Page("setrnost-2");
page.setPrevId("setrnost");
page.setNextId("setrnost-3");
page.customInit = function ()
{
    selfPage = this;

    $("#" + this.getId() + " .popup-button").unbind().click(function () {
        $("#" + selfPage.getId() + " .body-image").addClass("final");
    });
    $("#" + this.getId() + " .next").unbind().click(function () {
        $("#" + selfPage.getId() + " .bar canvas").unbind();
        $("#" + selfPage.getId() + " .bcg .arrow").hide();
        $("#" + selfPage.getId() + " .next").hide();
        $("#" + selfPage.getId() + " .final-graph").animate({height: "225px"}, 1600, function () {
            $("#" + selfPage.getId() + " .numbers").fadeIn();
        });

    });

    $("#" + this.getId() + " .bar.first .user-bar-first").bars({
        fgColor: 'rgba(0,0,0,0)',
        'change': function (e) {
            selfPage.changeBarPosition("first", 272, 100);
        }
    });

    $("#" + this.getId() + " .bar.second .user-bar-second").bars({
        fgColor: 'rgba(0,0,0,0)',
        'change': function (e) {
            selfPage.changeBarPosition("second", 272, 100);
        }
    });

    $("#" + this.getId() + " .bar.third .user-bar-third").bars({
        fgColor: 'rgba(0,0,0,0)',
        'change': function (e) {
            selfPage.changeBarPosition("third", 272, 100);
        }
    });

    $("#" + this.getId() + " .bar.fourth .user-bar-fourth").bars({
        fgColor: 'rgba(0,0,0,0)',
        'change': function (e) {
            selfPage.changeBarPosition("fourth", 272, 100);
        }
    });

    $("#" + this.getId() + " .bar.fifth .user-bar-fifth").bars({
        fgColor: 'rgba(0,0,0,0)',
        'change': function (e) {
            selfPage.changeBarPosition("fifth", 272, 100);
        }
    });

    $("#" + this.getId() + " .bar.sixth .user-bar-sixth").bars({
        fgColor: 'rgba(0,0,0,0)',
        'change': function (e) {
            selfPage.changeBarPosition("sixth", 272, 100);
        }
    });
};
page.changeBarPosition = function (name, height, count)
{
    $("#" + this.getId() + " .bcg." + name + " .arrow").hide();
    number = (height / count) * ($("#" + this.getId() + " .user-bar-" + name + " input").val());
    $("#" + this.getId() + " .bcg." + name + " .image").css("height", number + "px");
};
page.reset = function ()
{
    $("#" + this.getId() + " .body-image").removeClass("final");
    $("#" + this.getId() + " .next").show();
    $("#" + this.getId() + " .final-graph").css("height", "0px");
    $("#" + this.getId() + " .numbers").hide();
    $("#" + this.getId() + " .bcg .arrow").show();

    $("#" + this.getId() + " .bar.first").html('<fieldset class="user-bar-first" data-width="47" data-displayInput=false data-height="272" data-cols="1" data-min="0" data-max="100"><input value=0></fieldset>');
    $("#" + this.getId() + " .bar.second").html('<fieldset class="user-bar-second" data-width="47" data-displayInput=false data-height="272" data-cols="1" data-min="0" data-max="100"><input value=0></fieldset>');
    $("#" + this.getId() + " .bar.third").html('<fieldset class="user-bar-third" data-width="47" data-displayInput=false data-height="272" data-cols="1" data-min="0" data-max="100"><input value=0></fieldset>');
    $("#" + this.getId() + " .bar.fourth").html('<fieldset class="user-bar-fourth" data-width="47" data-displayInput=false data-height="272" data-cols="1" data-min="0" data-max="100"><input value=0></fieldset>');
    $("#" + this.getId() + " .bar.fifth").html('<fieldset class="user-bar-fifth" data-width="47" data-displayInput=false data-height="272" data-cols="1" data-min="0" data-max="100"><input value=0></fieldset>');
    $("#" + this.getId() + " .bar.sixth").html('<fieldset class="user-bar-sixth" data-width="47" data-displayInput=false data-height="272" data-cols="1" data-min="0" data-max="100"><input value=0></fieldset>');

    $("#" + this.getId() + " .first .image").css("height", "163px");
    $("#" + this.getId() + " .second .image").css("height", "185px");
    $("#" + this.getId() + " .third .image").css("height", "150px");
    $("#" + this.getId() + " .fourth .image").css("height", "199px");
    $("#" + this.getId() + " .fifth .image").css("height", "209px");
    $("#" + this.getId() + " .seixth .image").css("height", "150px");
};
app.addNewPage(page);
// Setrnost 3
page = new TOCHR.Page("setrnost-3");
page.setPrevId("setrnost-2");
page.setNextId("indikacni-omezeni");
page.customInit = function ()
{
    selfPage = this;

    $("#" + this.getId() + " .popup-button").unbind().click(function () {
        $("#" + selfPage.getId() + " .body-image").addClass("final");
    });
    $("#" + this.getId() + " .next").unbind().click(function () {
        $("#" + selfPage.getId() + " .bar canvas").unbind();
        $("#" + selfPage.getId() + " .bcg .arrow").hide();
        $("#" + selfPage.getId() + " .next").hide();
        $("#" + selfPage.getId() + " .final-graph").animate({height: "218px"}, 1600, function () {
            $("#" + selfPage.getId() + " .numbers").fadeIn();
        });

    });


    $("#" + this.getId() + " .bar.first .user-bar-first").bars({
        fgColor: 'rgba(0,0,0,0)',
        'change': function (e) {
            selfPage.changeBarPosition("first", 272, 100);
        }
    });

    $("#" + this.getId() + " .bar.second .user-bar-second").bars({
        fgColor: 'rgba(0,0,0,0)',
        'change': function (e) {
            selfPage.changeBarPosition("second", 272, 100);
        }
    });

    $("#" + this.getId() + " .bar.third .user-bar-third").bars({
        fgColor: 'rgba(0,0,0,0)',
        'change': function (e) {
            selfPage.changeBarPosition("third", 272, 100);
        }
    });

    $("#" + this.getId() + " .bar.fourth .user-bar-fourth").bars({
        fgColor: 'rgba(0,0,0,0)',
        'change': function (e) {
            selfPage.changeBarPosition("fourth", 272, 100);
        }
    });

    $("#" + this.getId() + " .bar.fifth .user-bar-fifth").bars({
        fgColor: 'rgba(0,0,0,0)',
        'change': function (e) {
            selfPage.changeBarPosition("fifth", 272, 100);
        }
    });

    $("#" + this.getId() + " .bar.sixth .user-bar-sixth").bars({
        fgColor: 'rgba(0,0,0,0)',
        'change': function (e) {
            selfPage.changeBarPosition("sixth", 272, 100);
        }
    });


};
page.changeBarPosition = function (name, height, count)
{
    $("#" + this.getId() + " .bcg." + name + " .arrow").hide();
    number = height - ((height / count) * ($("#" + this.getId() + " .user-bar-" + name + " input").val()));
    $("#" + this.getId() + " .bcg." + name + " .image").css("height", number + "px");
};
page.reset = function ()
{
    $("#" + this.getId() + " .body-image").removeClass("final");
    $("#" + this.getId() + " .next").show();
    $("#" + this.getId() + " .final-graph").css("height", "0px");
    $("#" + this.getId() + " .numbers").hide();
    $("#" + this.getId() + " .bcg .arrow").show();

    $("#" + this.getId() + " .bar.first").html('<fieldset class="user-bar-first" data-width="47" data-displayInput=false data-height="272" data-cols="1" data-min="0" data-max="100"><input value=0></fieldset>');
    $("#" + this.getId() + " .bar.second").html('<fieldset class="user-bar-second" data-width="47" data-displayInput=false data-height="272" data-cols="1" data-min="0" data-max="100"><input value=0></fieldset>');
    $("#" + this.getId() + " .bar.third").html('<fieldset class="user-bar-third" data-width="47" data-displayInput=false data-height="272" data-cols="1" data-min="0" data-max="100"><input value=0></fieldset>');
    $("#" + this.getId() + " .bar.fourth").html('<fieldset class="user-bar-fourth" data-width="47" data-displayInput=false data-height="272" data-cols="1" data-min="0" data-max="100"><input value=0></fieldset>');
    $("#" + this.getId() + " .bar.fifth").html('<fieldset class="user-bar-fifth" data-width="47" data-displayInput=false data-height="272" data-cols="1" data-min="0" data-max="100"><input value=0></fieldset>');
    $("#" + this.getId() + " .bar.sixth").html('<fieldset class="user-bar-sixth" data-width="47" data-displayInput=false data-height="272" data-cols="1" data-min="0" data-max="100"><input value=0></fieldset>');

    $("#" + this.getId() + " .first .image").css("height", "172px");
    $("#" + this.getId() + " .second .image").css("height", "156px");
    $("#" + this.getId() + " .third .image").css("height", "188px");
    $("#" + this.getId() + " .fourth .image").css("height", "156px");
    $("#" + this.getId() + " .fifth .image").css("height", "174px");
    $("#" + this.getId() + " .seixth .image").css("height", "182px");
};
app.addNewPage(page);
// Indikacni omezeni
// Indikacni omezeni 1
page = new TOCHR.Page("indikacni-omezeni");
page.setPrevId("setrnost-3");
page.setNextId("indikacni-omezeni-2");
page.customInit = function ()
{
    selfPage = this;
    $("#" + this.getId() + " .next").unbind().click(function () {
        $("#" + selfPage.getId() + " .next").hide();
        $("#" + selfPage.getId() + " .text").show();
    });
};
page.reset = function ()
{
    $("#" + this.getId() + " .text").hide();
    $("#" + this.getId() + " .next").show();
};
app.addNewPage(page);
// Indikacni omezeni 2
page = new TOCHR.Page("indikacni-omezeni-2");
page.setPrevId("indikacni-omezeni");
page.setNextId("preskripce");
app.addNewPage(page);
// Preskripce
// Preskripce 1
page = new TOCHR.Page("preskripce");
page.setPrevId("indikacni-omezeni-2");
page.setNextId("preskripce-2");
page.customInit = function ()
{
    selfPage = this;
    $("#" + this.getId() + " .next").unbind().click(function () {
        $("#" + selfPage.getId() + " .next").hide();
        $("#" + selfPage.getId() + " .text").show();
    });
};
page.reset = function ()
{
    $("#" + this.getId() + " .text").hide();
    $("#" + this.getId() + " .next").show();
};
app.addNewPage(page);
// Preskripce 2
page = new TOCHR.Page("preskripce-2");
page.setPrevId("preskripce");
page.setNextId("preskripce-3");
app.addNewPage(page);
// Preskripce 3
page = new TOCHR.Page("preskripce-3");
page.setPrevId("preskripce-2");
page.setNextId("preskripce-4");
app.addNewPage(page);
// Preskripce 3
page = new TOCHR.Page("preskripce-4");
page.setPrevId("preskripce-3");
//page.setNextId("reprinty");
app.addNewPage(page);

// Ostatni
// Reprinty
//page = new TOCHR.Page("reprinty");
//page.setPrevId("preskripce-3");
////page.setNextId("ostatni");
//page.customInit = function ()
//{
//    $(".menu .ostatni").addClass("active");
//};
//page.reset = function ()
//{
//    $(".menu .ostatni").removeClass("active");
//};
//app.addNewPage(page);

// Init of whole application
app.init();
app.createThumbSlider();


$('.send-email.spc-email').on("click touch", function () {
    cordova.plugins.email.open({
        attachments: [
            'file://spc/CZ_Targin_SmPC_5-40_20.9.2016.pdf'
        ]
    });
});

function sendEmailCallback() {

}

function utf8_to_b64(str) {
//    return window.btoa(encodeURIComponent(str));
    return window.btoa(unescape(encodeURIComponent(str)));
}

var Base64 = {

    // private property
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

    // public method for encoding
    encode: function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = Base64._utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
                    this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
                    this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

        }

        return output;
    },

    // public method for decoding
    decode: function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

        }

        output = Base64._utf8_decode(output);

        return output;

    },

    // private method for UTF-8 encoding
    _utf8_encode: function (string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            } else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            } else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },

    // private method for UTF-8 decoding
    _utf8_decode: function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while (i < utftext.length) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            } else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            } else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }

        }

        return string;
    }

};
