/**
 * @author Stéphane Roucheray, Pol Cámara
 * @extends jquery
 */

jQuery.fn.carousel = function(previous, next, drag, options){
	var sliderList = jQuery(this).children()[0];
	
	if (sliderList) {

		var left = 0;
		var sum = 0;		
		var increment = jQuery(sliderList).children().outerWidth(true);
		var elmnts = jQuery(sliderList).children();
		var numElmts = elmnts.length;
		var sizeFirstElmnt = increment;
		var shownInViewport = Math.round(jQuery(this).width() / sizeFirstElmnt);
		var firstElementOnViewPort = 1;
		var probableElementOnViewPort = 0;
		var isAnimating = false;
		var totalWidth = increment * numElmts;
		if (isNaN(parseInt(jQuery(sliderList).css("left").replace("px","")))) {
			jQuery(sliderList).css("left","0");
		}
		jQuery(sliderList).css("left",0);
		if (!(typeof drag == "boolean" &&  drag == false)) {
			jQuery(this)
			.drag("start",function(){
				left = parseInt(jQuery(sliderList).css("left").replace("px",""));
			})
			.drag("dragend", function() {
				left = parseInt(jQuery(sliderList).css("left").replace("px",""));
				probableElementOnViewPort = Math.abs(left/increment);
				firstElementOnViewPort = parseInt(probableElementOnViewPort);
				probableElementOnViewPort -= firstElementOnViewPort;
				if (probableElementOnViewPort > .5) {
					firstElementOnViewPort++;
				}

				jQuery(sliderList).animate({left: "-"+firstElementOnViewPort*increment+'px'});	
				$("#"+options+" ul li").removeClass("active");
				$("#"+options+" ul li:nth("+(firstElementOnViewPort + 1)+")").addClass("active");
			})
			.drag(function( ev, dd ){
				if (!isAnimating) {
					if (sum >= 0) {
						left -= totalWidth
					} else if (sum <= -totalWidth) {
						left += totalWidth;
					}
					sum = left + dd.deltaX;
					jQuery(sliderList).css({
						left: sum
					});
				}
			});
		}

		for (i = 0; i < shownInViewport; i++) {
			jQuery(sliderList).css('width',(numElmts+shownInViewport)*increment + increment + "px");
			jQuery(sliderList).append(jQuery(elmnts[i]).clone());
		}
		
		jQuery(previous).click(function(event){
			if (!isAnimating) {			
				left = parseInt(jQuery(sliderList).css("left").replace("px",""));
				firstElementOnViewPort = Math.abs(left/increment);
				if (left >= 0)
					jQuery(sliderList).css("left", (left-totalWidth)+"px") 

				if (firstElementOnViewPort == 0) {
					jQuery(sliderList).css('left', "-" + numElmts * sizeFirstElmnt + "px");
					firstElementOnViewPort = numElmts;
				}
				else {
					firstElementOnViewPort--;
				}
				
				jQuery(sliderList).animate({
					left: "+=" + increment,
					y: 0,
					queue: true
				}, "swing", function(){isAnimating = false;});
				isAnimating = true;
			}
			
		});
		
		jQuery(next).click(function(event){
			if (!isAnimating) {
				left = parseInt(jQuery(sliderList).css("left").replace("px",""));
				if (left <= -totalWidth) 	{
					jQuery(sliderList).css("left", (left+totalWidth)+"px")
				}
				firstElementOnViewPort = Math.abs(left/increment);

				if (firstElementOnViewPort > numElmts) {
					firstElementOnViewPort = 2;
					jQuery(sliderList).css('left', "0px");
				}
				else {
					firstElementOnViewPort++;
				}
				jQuery(sliderList).animate({
						left: "-=" + increment,
						y: 0,
						queue: true
					}, "swing", function(){isAnimating = false;});
				isAnimating = true;
			}	
		});
	}
};
