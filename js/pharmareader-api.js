var PRApi =
        {
            /**
             * 
             * @param string presentation_const
             */
            startLog: function (presentation_const)
            {
                console.log({"startLog": presentation_const});
            },
            /**
             * 
             * @param string presentation_const
             * @param string page
             * @param string block
             * @param string action
             * @param string value
             */
            addLog: function (presentation_const, page, block, action, value)
            {
                if (!PRApi.isTest())
                {
//                     console.log({"addLog": [presentation_const, page, block, action, value]});
                    if (value==undefined)
                        console.log(presentation_const.concat(' %cpage:%c ',page, ' %c block:%c ', block, ' %c action:%c ', action, ' '),
                            'color:gray', 'font-weight: bold; background-color: #FFFF66;',
                            'color:gray', 'font-weight: bold; background-color: #FFCC66;',
                            'color:gray', 'font-weight: bold; background-color: #00CCFF;');
                    else  // zm�ny v parametrech naho�e prom�tnout dol� :-)
                        console.log(presentation_const.concat(' %cpage:%c ',page, ' %c block:%c ', block, ' %c action:%c ', action, ' %c value:%c ', value),
                            'color:gray', 'font-weight: bold; background-color: #FFFF66;',
                            'color:gray', 'font-weight: bold; background-color: #FFCC66;',
                            'color:gray', 'font-weight: bold; background-color: #00CCFF;',
                            'color:gray', 'font-weight: bold; ');
                }
                else
                {
                    console.log({"addLog": 'training'});
                }
            },
            /**
             * 
             */
            toPR: function ()
            {
                console.log('Redirect to PR');
                alert('Redirect to PR');
            },
            /**
             * 
             * @returns {Boolean}
             */
            isTest: function ()
            {
                return false;
            }
        };